import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Idea} from '../models/Idea';

@Component({
  selector: 'app-idea-display',
  templateUrl: './idea-display.component.html',
  styleUrls: ['./idea-display.component.scss']
})
export class IdeaDisplayComponent {

  @Output() openIdea: EventEmitter<number> = new EventEmitter<number>();
  @Output() doDeleteIdea: EventEmitter<number> = new EventEmitter<number>();

  @Input() idea: Idea;

  deleteIdea(event: MouseEvent = null): void {
    if (event) {
      event.stopImmediatePropagation();
    }
    this.doDeleteIdea.emit(this.idea.id);
  }

}
