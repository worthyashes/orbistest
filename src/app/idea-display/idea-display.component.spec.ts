import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdeaDisplayComponent } from './idea-display.component';
import {Idea} from '../models/Idea';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('IdeaDisplayComponent', () => {
  let component: IdeaDisplayComponent;
  let fixture: ComponentFixture<IdeaDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IdeaDisplayComponent ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdeaDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('deleteIdea', () => {
    it('should trigger the event emitter', () => {
      spyOn(component.doDeleteIdea, 'emit');
      component.idea = { id: 123 } as Idea;
      component.deleteIdea();
      expect(component.doDeleteIdea.emit).toHaveBeenCalledWith(123);
    });
  });
});
