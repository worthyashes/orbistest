import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {Idea} from '../models/Idea';
import {Store} from '@ngrx/store';
import {AppState} from '../state/reducers';
import {selectIdea} from '../state/reducers/ideas.reducer';
import {take} from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class IdeaResolver implements Resolve<Observable<Idea>> {
  constructor(private store: Store<AppState>) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Idea> {
    return this.store.select(selectIdea(parseInt(route.paramMap.get('id'), 10))).pipe(take(1));
  }
}
