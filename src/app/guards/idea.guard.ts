import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {select, Store} from '@ngrx/store';
import {AppState} from '../state/reducers';
import {map, switchMap, take, withLatestFrom} from 'rxjs/operators';
import {selectIdea} from '../state/reducers/ideas.reducer';
import * as _ from 'lodash';
@Injectable({
  providedIn: 'root'
})
export class IdeaGuard implements CanActivate {

  constructor(private store: Store<AppState>,
              private router: Router) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.store.select(selectIdea(parseInt(next.paramMap.get('id'), 10))).pipe(
      take(1),
      map(idea => {
        if (idea === undefined) {
          return this.router.createUrlTree(['/']);
        } else {
          return true;
        }
      })
    );
  }
}
