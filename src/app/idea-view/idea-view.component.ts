import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {Idea} from '../models/Idea';
import {Store} from '@ngrx/store';
import {AppState} from '../state/reducers';
import * as ideaActions from '../state/actions/idea.actions';

@Component({
  selector: 'app-idea-view',
  templateUrl: './idea-view.component.html',
  styleUrls: ['./idea-view.component.scss']
})
export class IdeaViewComponent implements OnInit {

  idea: Idea;

  constructor(private activatedRoute: ActivatedRoute, private store: Store<AppState>, private router: Router) { }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((data: { idea: Idea }) => {
      this.idea = data.idea;
    });
  }

  saveIdea(idea: Idea): void {
    this.store.dispatch(ideaActions.saveIdea( { idea } ));
    this.navigateToHome();
  }

  doIdeaDelete(id: number): void {
    this.store.dispatch(ideaActions.deleteIdea({ id }));
    this.navigateToHome();
  }

  navigateToHome(): void {
    this.router.navigate(['/']);
  }

}
