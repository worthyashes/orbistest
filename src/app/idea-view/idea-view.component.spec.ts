import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdeaViewComponent } from './idea-view.component';
import {RouterTestingModule} from '@angular/router/testing';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {of} from 'rxjs';
import {Idea} from '../models/Idea';
import * as ideaActions from '../state/actions/idea.actions';

describe('IdeaViewComponent', () => {
  let component: IdeaViewComponent;
  let fixture: ComponentFixture<IdeaViewComponent>;
  let mockStore: MockStore;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      providers: [
        provideMockStore(),
        { provide: ActivatedRoute, useValue: { data: of({ idea: { title: 'Test', description: 'Data'} as Idea }) } }
      ],
      declarations: [ IdeaViewComponent ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdeaViewComponent);
    component = fixture.componentInstance;
    mockStore = TestBed.inject(MockStore);
    router = TestBed.inject(Router);
    spyOn(mockStore, 'dispatch');
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('should get the injected idea', () => {
      component.ngOnInit();
      expect(component.idea.title).toBe('Test');
      expect(component.idea.description).toBeTruthy('Data');
    });
  });

  describe('saveIdea', () => {
    it('should dispatch a save idea action', () => {
      spyOn(component, 'navigateToHome');
      const idea = {
        title: 'Hello',
        description: 'World'
      } as Idea;
      component.saveIdea(idea);
      expect(mockStore.dispatch).toHaveBeenCalledWith(ideaActions.saveIdea( {idea} ));
      expect(component.navigateToHome).toHaveBeenCalled();
    });
  });

  describe('doIdeaDelete', () => {
    it('should dispatch a delete idea action', () => {
      spyOn(component, 'navigateToHome');
      component.doIdeaDelete(123);
      expect(mockStore.dispatch).toHaveBeenCalledWith(ideaActions.deleteIdea( { id: 123 }));
      expect(component.navigateToHome).toHaveBeenCalled();
    });
  });

  describe('navigateToHome', () => {
    it('should trigger a navigaiton', () => {
      spyOn(router, 'navigate');
      component.navigateToHome();
      expect(router.navigate).toHaveBeenCalledWith(['/']);
    });
  });
});
