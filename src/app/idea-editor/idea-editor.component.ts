import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Idea} from '../models/Idea';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-idea-editor',
  templateUrl: './idea-editor.component.html',
  styleUrls: ['./idea-editor.component.scss']
})
export class IdeaEditorComponent implements OnInit {

  @Input() idea: Idea;

  @Output() saveIdea: EventEmitter<Idea> = new EventEmitter<Idea>();
  @Output() deleteIdea: EventEmitter<number> = new EventEmitter<number>();
  @Output() doBack: EventEmitter<void> = new EventEmitter<void>();

  addIdeaForm: FormGroup;

  remainingCount = 140;

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.createForm();
    if (this.idea) {
      this.patchForm();
    }
  }

  calculateRemainingCount(totalCount, existingCount = 0): void {
    this.remainingCount = totalCount - existingCount;
  }

  patchForm(): void {
    this.addIdeaForm.patchValue({
      title: this.idea.title,
      description: this.idea.description,
      createdDate: this.idea.createdDate,
      updatedDate: this.idea.updatedDate,
      id: this.idea.id
    });
  }

  delete(): void {
    this.deleteIdea.emit(this.idea.id);
  }

  submitForm(): void {
    const idea = {
      title: this.addIdeaForm.controls.title.value,
      description: this.addIdeaForm.controls.description.value,
      createdDate: this.addIdeaForm.controls.createdDate.value,
      id: this.addIdeaForm.controls.id.value
    } as Idea;
    idea.updatedDate = new Date().toISOString();
    if (!idea.createdDate) {
      idea.createdDate = new Date().toISOString();
    }
    this.saveIdea.emit(idea);
    this.addIdeaForm.reset();
  }

  editMode(): boolean {
    if (!this.idea && !this.idea?.id) {
      return false;
    }
    return true;
  }

  private createForm(): void {
    this.addIdeaForm = this.fb.group(
      {
        title: [
          '',
          Validators.required
        ],
        description: [
          '',
          [
            Validators.maxLength(140),
            Validators.required
          ]
        ],
        updatedDate: [],
        createdDate: [],
        id: []
      }
    );
    this.addIdeaForm.controls.description.valueChanges.subscribe(
      value => {
        this.calculateRemainingCount(140, value?.length);
      });
  }

}
