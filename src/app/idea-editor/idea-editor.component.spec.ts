import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdeaEditorComponent } from './idea-editor.component';
import {ReactiveFormsModule} from '@angular/forms';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {Idea} from '../models/Idea';

describe('IdeaEditorComponent', () => {
  let component: IdeaEditorComponent;
  let fixture: ComponentFixture<IdeaEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule
      ],
      declarations: [ IdeaEditorComponent ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdeaEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('should patch the form if the idea exists', () => {
      spyOn(component, 'patchForm');
      component.idea = {} as Idea;
      component.ngOnInit();
      expect(component.patchForm).toHaveBeenCalled();
    });
  });

  describe('calculateRemainingCount', () => {
    it('should return the remaining count', () => {
      component.calculateRemainingCount( 100, 10);
      expect(component.remainingCount).toBe(90);
      component.calculateRemainingCount( 10, 10);
      expect(component.remainingCount).toBe(0);
    });
  });

  describe('patchForm', () => {
    it('should populate the form', () => {
      component.idea = {
        title: 'Hello',
        description: 'World',
        createdDate: 'CreatedDate',
        updatedDate: 'UpdatedDate',
        id: 123
      } as Idea;
      component.ngOnInit();
      expect(component.addIdeaForm.controls.title.value).toBe('Hello');
      expect(component.addIdeaForm.controls.description.value).toBe('World');
      expect(component.addIdeaForm.controls.createdDate.value).toBe('CreatedDate');
      expect(component.addIdeaForm.controls.updatedDate.value).toBe('UpdatedDate');
      expect(component.addIdeaForm.controls.id.value).toBe(123);
    });
  });

  describe('delete', () => {
    it('should emit from the delete emitter', () => {
      spyOn(component.deleteIdea, 'emit');
      component.idea = { id: 123 } as Idea;
      component.delete();
      expect(component.deleteIdea.emit).toHaveBeenCalledWith(123);
    });
  });

  // Intermitent failure with this test as the dates may be out by a few milliseconds
  xdescribe('submit form', () => {
    it('should dispatch idea from the form with null id', () => {
      spyOn(component.saveIdea, 'emit');
      spyOn(component.addIdeaForm, 'reset');
      component.ngOnInit();
      component.addIdeaForm.controls.title.setValue('New Idea');
      component.addIdeaForm.controls.description.setValue('From form');
      component.submitForm();
      expect(component.saveIdea.emit).toHaveBeenCalledWith({
        title: 'New Idea',
        description: 'From form',
        createdDate: new Date().toISOString(),
        updatedDate: new Date().toISOString(),
        id: null
      } as Idea);
    });
  });

  describe('editMode', () => {
    it('is not in edit mode', () => {
      expect(component.editMode()).toBeFalse();
    });
    it('is in edit mode', () => {
      component.idea = { id: 123 } as Idea;
      expect(component.editMode()).toBeTrue();
    });
  });
});
