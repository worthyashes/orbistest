import { Component, OnInit } from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../state/reducers';
import * as ideaActions from '../state/actions/idea.actions';
import {selectIdeas} from '../state/reducers/ideas.reducer';
import {Idea} from '../models/Idea';
import {Order} from '../enum/order.enum';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

  ideas$ = this.store.select(selectIdeas);

  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
  }

  addIdea(idea: Idea): void {
    this.store.dispatch(ideaActions.addIdea({ idea }));
  }

  doChangeSort(order: Order): void {
    this.store.dispatch(ideaActions.updateSort({ order }));
  }

  doIdeaDelete(id: number): void {
    this.store.dispatch(ideaActions.deleteIdea({ id }));
  }

}
