import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LandingComponent} from './landing.component';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import * as ideaActions from '../state/actions/idea.actions';
import {Idea} from '../models/Idea';
import {Order} from '../enum/order.enum';

describe('LandingComponent', () => {
  let component: LandingComponent;
  let fixture: ComponentFixture<LandingComponent>;
  let mockStore: MockStore;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        provideMockStore()
      ],
      declarations: [ LandingComponent ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingComponent);
    component = fixture.componentInstance;
    mockStore = TestBed.inject(MockStore);
    spyOn(mockStore, 'dispatch');
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('addIdea', () => {
    it('should dispatch an add action', () => {
      const idea = {
        title: 'Hello World'
      } as Idea;
      component.addIdea(idea);
      expect(mockStore.dispatch).toHaveBeenCalledWith(ideaActions.addIdea({ idea }));
    });
  });

  describe('doChangeSort', () => {
    it('should dispatch an order update action', () => {
      component.doChangeSort(Order.alphabetically);
      expect(mockStore.dispatch).toHaveBeenCalledWith(ideaActions.updateSort( { order: Order.alphabetically }));
    });
  });

  describe('doIdeaDelete', () => {
    it('should dispatch a delete action', () => {
      component.doIdeaDelete(123);
      expect(mockStore.dispatch).toHaveBeenCalledWith(ideaActions.deleteIdea({ id: 123 }));
    });
  });
});
