import { createAction, props } from '@ngrx/store';
import {Idea} from '../../models/Idea';
import {Order} from '../../enum/order.enum';

export const addIdea = createAction(
  '[Idea] Add Idea',
  props<{ idea: Idea }>()
);

export const addIdeaComplete = createAction(
  '[Idea] Add Idea Complete',
  props<{ idea: Idea }>()
);

export const saveIdea = createAction(
  '[Idea] Save Idea',
  props<{ idea: Idea }>()
);

export const updateSort = createAction(
  '[Idea] Update Sort',
  props<{ order: Order }>()
);

export const deleteIdea = createAction(
  '[Idea] Delete Idea',
  props<{ id: number }>()
);
