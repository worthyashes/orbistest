import * as fromIdea from './idea.actions';
import {Idea} from '../../models/Idea';
import {Order} from '../../enum/order.enum';

describe('addIdea', () => {
  it('should return an action', () => {
    expect(fromIdea.addIdea({ idea: {} as Idea }).type).toBe('[Idea] Add Idea');
  });
});

describe('addIdeaComplete', () => {
  it('should return an action', () => {
    expect(fromIdea.addIdeaComplete({ idea: {} as Idea }).type).toBe('[Idea] Add Idea Complete');
  });
});

describe('saveIdea', () => {
  it('should return an action', () => {
    expect(fromIdea.saveIdea({ idea: {} as Idea }).type).toBe('[Idea] Save Idea');
  });
});

describe('updateSort', () => {
  it('should return an action', () => {
    expect(fromIdea.updateSort({ order: Order.alphabetically }).type).toBe('[Idea] Update Sort');
  });
});

describe('deleteIdea', () => {
  it('should return an action', () => {
    expect(fromIdea.deleteIdea({ id: 0 }).type).toBe('[Idea] Delete Idea');
  });
});
