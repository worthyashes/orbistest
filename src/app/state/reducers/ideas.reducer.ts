import {createReducer, createSelector, on} from '@ngrx/store';
import {Idea} from '../../models/Idea';
import * as ideaActions from '../actions/idea.actions';
import {AppState} from './index';
import {Order} from '../../enum/order.enum';

export interface IdeaState {
  ideas: Idea[];
  order: string;
}

export const initialState: IdeaState = {
  ideas: [],
  order: Order.createdDate
};

export const reducer = createReducer(
  initialState,
  on(ideaActions.addIdeaComplete, (state, {idea}) => (
    {
      ...state,
      ideas: [
        ...state.ideas,
        idea
      ]
    }
  )),
  on(ideaActions.saveIdea, (state, { idea }) => (
    {
      ...state,
      ideas: state.ideas.map(value => value.id === idea.id ? { ...idea } : value)
    }
  )),
  on(ideaActions.updateSort, (state, { order }) => (
    {
      ...state,
      order
    }
  )),
  on(ideaActions.deleteIdea, (state, { id }) => (
    {
      ...state,
      ideas: state.ideas.filter(value => value.id !== id)
    }
  ))
);

// Sorters
export const sortByCreatedDate = (a: Idea, b: Idea) => {
  if (Date.parse(a.createdDate) < Date.parse(b.createdDate)) {
    return -1;
  }
  if (Date.parse(a.createdDate) > Date.parse(b.createdDate)) {
    return 1;
  }
  return 0;
};

export const sortAlphabetically = (a: Idea, b: Idea) => {
  if (a.title.toUpperCase() < b.title.toUpperCase()) {
    return -1;
  }
  if (a.title.toUpperCase() > b.title.toUpperCase()) {
    return 1;
  }
  return 0;
};

// Selectors
export const selectIdeaState = (state: AppState) => state.ideaState;

export const selectOrder = createSelector(
  selectIdeaState,
  (ideaState) => ideaState?.order
);

export const selectIdeas = createSelector(
  selectIdeaState,
  selectOrder,
  (ideaState, order) => {
    if (order === Order.createdDate) {
      return ideaState.ideas.slice().sort(sortByCreatedDate);
    } else if (order === Order.alphabetically) {
      return ideaState.ideas.slice().sort(sortAlphabetically);
    }
  }
);

export const selectIdeaCount = createSelector(
  selectIdeas,
  (ideas) => ideas.length
);

export const selectIdea = (id: number) => createSelector(
  selectIdeas,
  (ideas) => {
    return ideas.filter(idea => idea.id === id)[0];
  }
);

