import {
  reducer,
  initialState,
  sortByCreatedDate,
  sortAlphabetically,
  IdeaState,
  selectIdeas,
  selectIdeaCount,
  selectIdea
} from './ideas.reducer';
import * as ideaActions from '../actions/idea.actions';
import {Idea} from '../../models/Idea';
import {Order} from '../../enum/order.enum';

describe('Ideas Reducer', () => {
  describe('an unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });

  describe('addIdeaComplete', () => {
    it('should add an idea', () => {
      const idea = {
        id: 123
      } as Idea;
      const action = ideaActions.addIdeaComplete( { idea });

      const result = reducer(initialState, action);

      expect(result.ideas.filter(value => value.id === 123).length).toBe(1);
    });
  });

  describe('saveIdea', () => {
    it('should save the idea', () => {
      initialState.ideas.push({ id: 111, title: 'Hello' } as Idea);

      const idea = {
        id: 111,
        title: 'World'
      } as Idea;
      const action = ideaActions.saveIdea({ idea });

      const result = reducer(initialState, action);

      const savedIdea: Idea = result.ideas.filter(value => value.id === 111)[0];
      expect(savedIdea.title).toBe('World');
    });
  });

  describe('updateSort', () => {
    it('should update the sort', () => {
      const action = ideaActions.updateSort({ order: Order.alphabetically });

      const result = reducer(initialState, action);

      expect(result.order).toBe(Order.alphabetically);
    });
  });

  describe('deleteIdea', () => {
    it('should delete the idea', () => {
      initialState.ideas.push({ id: 222, title: 'I should be deleted' } as Idea);

      const action = ideaActions.deleteIdea({ id: 222 });

      const result = reducer(initialState, action);

      expect(result.ideas.filter(value => value.id === 222).length).toBe(0);
    });
  });

  describe('sortByCreatedDate', () => {
    it('should return a -1', () => {
      const oldDate = new Date(1980);
      const youngDate = new Date();
      const a = {
        createdDate: oldDate.toISOString()
      } as Idea;
      const b = {
        createdDate: youngDate.toISOString()
      } as Idea;
      expect(sortByCreatedDate(a, b)).toBe(-1);
    });

    it('should return a 1', () => {
      const oldDate = new Date(1980);
      const youngDate = new Date();
      const a = {
        createdDate: youngDate.toISOString()
      } as Idea;
      const b = {
        createdDate: oldDate.toISOString()
      } as Idea;
      expect(sortByCreatedDate(a, b)).toBe(1);
    });

    it('should return a 0', () => {
      const date = new Date();
      const a = {
        createdDate: date.toISOString()
      } as Idea;
      const b = {
        createdDate: date.toISOString()
      } as Idea;
      expect(sortByCreatedDate(a, b)).toBe(0);
    });
  });

  describe('sortAlphabetically', () => {
    it('should return 1', () => {
      const a = {
        title: 'b'
      } as Idea;
      const b = {
        title: 'a'
      } as Idea;
      expect(sortAlphabetically(a, b)).toBe(1);
    });

    it('should return -1', () => {
      const a = {
        title: 'a'
      } as Idea;
      const b = {
        title: 'b'
      } as Idea;
      expect(sortAlphabetically(a, b)).toBe(-1);
    });

    it('should return 0', () => {
      const a = {
        title: 'a'
      } as Idea;
      const b = {
        title: 'a'
      } as Idea;
      expect(sortAlphabetically(a, b)).toBe(0);
    });
  });

  describe('selectIdeas', () => {
    it('should return the ideas in the correct date order', () => {
      const oldDate = new Date(1980);
      const youngDate = new Date();
      const state = {
        ideas: [
          {
            createdDate: youngDate.toISOString(),
            title: 'Second'
          } as Idea,
          {
            createdDate: oldDate.toISOString(),
            title: 'First'
          } as Idea,
        ]
      } as IdeaState;
      const result = selectIdeas.projector(state, Order.createdDate);
      expect(result[0].title).toBe('First');
      expect(result[1].title).toBe('Second');
    });
    it('should return the ideas in the correct alphabetical order', () => {
      const state = {
        ideas: [
          {
            title: 'b'
          } as Idea,
          {
            title: 'a'
          } as Idea,
        ]
      } as IdeaState;
      const result = selectIdeas.projector(state, Order.alphabetically);
      expect(result[0].title).toBe('a');
      expect(result[1].title).toBe('b');
    });
  });

  describe('selectIdeaCount', () => {
    it('should return the correct count', () => {
      const ideas = [
          {
            title: 'b'
          } as Idea,
          {
            title: 'a'
          } as Idea,
        ];
      expect(selectIdeaCount.projector(ideas)).toBe(2);
    });
  });

  describe('selectIdea', () => {
    it('should return the correct idea', () => {
      const ideas = [
        {
          title: 'Choose me!',
          id: 123
        } as Idea,
        {
          title: 'Not me',
          id: 321
        } as Idea
      ];
      const result = selectIdea(123).projector(ideas);
      expect(result.title).toBe('Choose me!');
    });
  });

});
