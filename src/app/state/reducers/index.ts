import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../../environments/environment';
import {IdeaState} from './ideas.reducer';
import * as idea from './ideas.reducer';

export interface AppState {
  ideaState: IdeaState;
}

export const reducers: ActionReducerMap<AppState> = {
  ideaState: idea.reducer
};


export const metaReducers: MetaReducer<AppState>[] = !environment.production ? [] : [];
