import { Injectable } from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import * as ideaActions from '../actions/idea.actions';
import {AppState} from '../reducers';
import {catchError, map, withLatestFrom} from 'rxjs/operators';
import {selectIdeaCount} from '../reducers/ideas.reducer';
import {select, Store} from '@ngrx/store';
import * as _ from 'lodash';

@Injectable()
export class IdeaEffects {

  addIdea$ = createEffect(() => this.actions$.pipe(
    ofType(ideaActions.addIdea),
    withLatestFrom(this.store.pipe(select(selectIdeaCount))),
    map(([action, count]) => {
      // To create an id we grab the count of the ideas array.
      const idea = _.clone(action.idea);
      // The count is 0 indexed so dinking it on to make a more sensible id
      idea.id = ++count;
      return ideaActions.addIdeaComplete({ idea });
    })
  ));

  constructor(private actions$: Actions, private store: Store<AppState>) {}

}
