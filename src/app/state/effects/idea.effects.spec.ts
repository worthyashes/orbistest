import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { IdeaEffects } from './idea.effects';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import * as ideaActions from '../actions/idea.actions';
import {Action, MemoizedSelector} from '@ngrx/store';
import {Idea} from '../../models/Idea';
import {selectIdeaCount} from '../reducers/ideas.reducer';
import {AppState} from '../reducers';
import {cold, hot} from 'jasmine-marbles';

describe('IdeaEffects', () => {
  let actions$: Observable<Action>;
  let effects: IdeaEffects;

  let mockSelectIdeaCount: MemoizedSelector<AppState, number>;
  let mockStore: MockStore;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        IdeaEffects,
        provideMockActions(() => actions$),
        provideMockStore()
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    });

    effects = TestBed.inject(IdeaEffects);
    mockStore = TestBed.inject(MockStore);
    mockSelectIdeaCount = mockStore.overrideSelector(
      selectIdeaCount,
      2
    );
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  describe('addIdea$', () => {
    it('should add an id and dispatch action', () => {
      const idea = {
        title: 'New idea'
      } as Idea;
      const addIdeaAction = ideaActions.addIdea({ idea });

      const expectedResult = ideaActions.addIdeaComplete( {
        idea: {
          title: 'New idea',
          id: 3
        } as Idea
      });

      const sourceAction$ = hot('-a', { a: addIdeaAction });
      const expected$ = cold('-c', { c: expectedResult });

      actions$ = sourceAction$;

      expect(effects.addIdea$).toBeObservable(expected$);

    });
  });
});
