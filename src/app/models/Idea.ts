export interface Idea {
  id: number;
  title: string;
  description: string;
  createdDate: string;
  updatedDate: string;
}
