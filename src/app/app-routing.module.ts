import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LandingComponent} from './landing/landing.component';
import {IdeaViewComponent} from './idea-view/idea-view.component';
import {IdeaResolver} from './resolvers/IdeaResolver';
import {IdeaGuard} from './guards/idea.guard';

const routes: Routes = [
  {
    path: '',
    component: LandingComponent
  },
  {
    path: 'idea/:id',
    component: IdeaViewComponent,
    resolve: {
      idea: IdeaResolver
    },
    canActivate: [IdeaGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
