import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Idea} from '../models/Idea';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Order} from '../enum/order.enum';

@Component({
  selector: 'app-idea-list',
  templateUrl: './idea-list.component.html',
  styleUrls: ['./idea-list.component.scss']
})
export class IdeaListComponent implements OnInit {

  order = Order;

  @Input() ideas: Idea[];
  @Output() changeSort: EventEmitter<Order> = new EventEmitter<Order>();
  @Output() deleteIdea: EventEmitter<number> = new EventEmitter<number>();
  sortForm: FormGroup;

  constructor(private router: Router, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.createForm();
  }

  openIdea(id): void {
    this.router.navigate([`/idea/${id}`]);
  }

  private createForm(): void {
    this.sortForm = this.fb.group({
      sorter: ['createdDate']
    });
    this.sortForm.controls.sorter.valueChanges.subscribe(
      value => {
        this.changeSort.emit(value);
      }
    );
  }


}
