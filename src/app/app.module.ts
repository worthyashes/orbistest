import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LandingComponent } from './landing/landing.component';
import { IdeaListComponent } from './idea-list/idea-list.component';
import { IdeaViewComponent } from './idea-view/idea-view.component';
import {ReactiveFormsModule} from '@angular/forms';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {environment} from '../environments/environment';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './state/reducers';
import { EffectsModule } from '@ngrx/effects';
import { IdeaEffects } from './state/effects/idea.effects';
import { IdeaEditorComponent } from './idea-editor/idea-editor.component';
import { IdeaDisplayComponent } from './idea-display/idea-display.component';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LandingComponent,
    IdeaListComponent,
    IdeaViewComponent,
    IdeaEditorComponent,
    IdeaDisplayComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    StoreModule.forRoot(reducers, {
      metaReducers
    }),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
    EffectsModule.forRoot([IdeaEffects])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
