# Orbis Idea Board

## Thoughts

I used NGRX as the data source for this application. Clearly this is over engineered, but I
wanted to demonstrate that I can use this technology.

I like to load data in resolvers so the data is in place before routing occurs. Clearly in this
case as we have the data in memory there isn't really a need for this but again I wanted 
to show the technique.

I also like to protect routes using guards to protect routes where they are not available
to be accessed either because the data is not available or the user doesn't have sufficient
permissions to see that page.

I made the decision to have a separate edit screen to show the routing techniques I explained above.

## Standard instructions below

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.6.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
